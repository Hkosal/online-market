import React from 'react';
import './App.scss';
import Navbar from './components/layouts/shared/Navbar';
import Home from './components/layouts/pages/home'
import About from './components/layouts/pages/About';
import Shop from './components/layouts/pages/Shop';
import{BrowserRouter as Router, Route} from 'react-router-dom';
import Detail from './components/layouts/pages/detail';
import Footer from './components/layouts/shared/Footer';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Route path="/" exact component={Home} />
        <Route path="/about" component={About}/>
        <Route path="/shop" component={Shop}/> 
        <Route path="/detail" component={Detail} />
        <Footer />
      </div>
    </Router>
  );
}
// const Home = () =>(
//   <div>
//     <h1>Home page</h1>
//   </div>
// )
export default App;
