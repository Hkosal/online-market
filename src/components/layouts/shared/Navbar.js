import React from 'react';
import {Link} from 'react-router-dom';
import { BiShoppingBag } from 'react-icons/bi';

function Navbar() {
  return (
        <nav>
            <div className="menu">
                <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2019/04/Logo.png" alt="" />
            </div>
            <div className="menu">
                <Link to="/">
                    <button>Home</button>
                </Link>
                <Link to="/shop">
                    <button>Shop</button>
                </Link>
                <button>Media</button>
                <button>Electronic</button>
                <button>Page</button>
                <button>Blog</button>
            </div>
            <div className="menu">
                <button><i className="fa fa-bars"></i></button>
                <button><i className="fa fa-search"></i></button>
                <BiShoppingBag />
            </div>
        </nav>
  );
}

export default Navbar;
