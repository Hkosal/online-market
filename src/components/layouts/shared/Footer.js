import React from 'react'
import {FaRegEnvelope} from 'react-icons/fa'

function Footer() {
    return (
            <footer>
                <div class="content">
                    <div class="flex-footer">
                        <FaRegEnvelope />
                        <div class="flex-footer-desc">
                            <h3>NEWSLETTER</h3>
                            <p>Sign Up For Our Newsletter</p>
                        </div>
                        <form action="#">
                            <input type="text" placeholder="Your email address" />
                            <button>Subscribe</button>
                            
                        </form>
                    </div>
                    <div class="flex-footer">
                        <div class="flex-footer-desc">
                            <i className="fa fa-facebook"></i>
                            <i className="fa fa-facebook"></i>
                            <i className="fa fa-facebook"></i>
                            <i className="fa fa-facebook"></i>
                            <i className="fa fa-facebook"></i>
                        </div>
                    </div>
                </div>
            </footer>
    )
}

export default Footer
