import React, {useState} from 'react';
//slide import
import Slider from "react-slick";

// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

// Tabs import
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/swiper.scss';


function Home() {
  var slider = {
    autoplay : true,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  const [isShown, setIsShown] = useState(false);

  

  return (
    <div className="container">
        <div className="content">
          <div className="content-wrapper">
            <div className="slide">            
              <Slider {...slider}>
                  <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2019/04/slider-1.png" alt="" />
                  <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2019/04/slider-2.png" alt="" />
              </Slider>    
            </div>
            <div className="wrapper-col">
              <div className="tab">
                <h2>TOP PRODUCTS</h2>
                <Tabs>
                  <TabList>
                    <Tab>FEATURED</Tab>
                    <Tab>SPECIAL</Tab>
                    <Tab>BEST SELLER</Tab>
                  </TabList>

                  <TabPanel>
                    <div className="product-slider">
                      <Swiper
                        spaceBetween={10}
                        slidesPerView={4}
                      >
                        <SwiperSlide
                        onMouseOver={() => setIsShown(true)}
                        onMouseOut={() => setIsShown(false)}
                          > <img id="img" src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" />
                          {isShown 
                          // && (
                          // <div className="hover">
                          //   <button onClick={openModal}>pop up</button>
                          // </div>
                        // )
                        }</SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/24-290x435.jpg" alt="" /></SwiperSlide>
                                       
                      </Swiper>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <div className="product-slider">
                    <Swiper
                        spaceBetween={10}
                        slidesPerView={4}
                      >
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                        <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/7-290x435.jpg" alt="" /></SwiperSlide>
                      </Swiper>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <div className="product-slider">
                      <Swiper
                          spaceBetween={10}
                          slidesPerView={4}
                        >
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                          <SwiperSlide> <img src="https://demo.themedelights.com/Wordpress/WP01/WP006/wp-content/uploads/2017/12/5-290x435.jpg" alt="" /></SwiperSlide>
                      </Swiper>
                    </div>
                  </TabPanel>
                </Tabs>
              </div>
            </div>
          </div>
        </div>
    </div>
  );
}

export default Home;
